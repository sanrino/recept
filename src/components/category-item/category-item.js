import React from 'react';
import { Link } from 'react-router-dom';
import './category-item.css';

const CategoryItemPage = ({ category, itemName, recipes }) => {
    
    const categoryName = category.map( (item, key) => {
        if(`${item.link}` === itemName){
            return <h1 key={item.id}><span>{item.name}</span></h1>;
        }
        return null;
    });
    
    const categoryRecipes = recipes.map( (item, key) =>{
        if(`${item.categoryMain}` === itemName){
            return (
                <div className="recipe-item" key={key}>
                    <h4 key={item.id}><Link to={`/${item.categoryMain}/${item.categoryId}/${item.id}`}>{item.name}</Link></h4>
                    <div className="recipe-item-picture"><img src={item.imgLink} alt='some value'/></div>
                    <div className="recipe-item-category">
                        <Link to={`/${item.categoryMain}`}><span>{item.category}</span></Link>
                        <Link to={`/${item.categoryMain}/${item.categoryId}`}><span> • {item.childСategory}</span></Link>
                    </div>
                    <div className="recipe-item-description"><strong>Время приготовления: </strong> <br/> {item.descriptionShort}</div>
                </div>
            );
        }
        return null;
    }); 

    return(
        <div className="category-page">
            {categoryName}
            <div className="recipe-wrapper"> {categoryRecipes}</div>
        </div>
    )
};
export default CategoryItemPage;