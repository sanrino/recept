import React from 'react';
import { Link } from "react-router-dom";
import './category.css';

const Category = ({ category, itemCategory, recipes }) => {
    
    const categoryName = category.map(function(item) {

        const nameItems = item.items;

        if(nameItems){
            return nameItems.map((item, index) => {
                if(`${item.categoryName}` === itemCategory){
                    return (
                        <div className="top-panel" key={index}>
                            <h1 key={item.id}><span>{item.name}</span></h1>
                            <p>{item.description}</p>
                        </div>
                    );
                }
                return null;
            });
        }
        return null;
    });
    
    const categoryrecipe = recipes.map(function(item, index){
        if(`${item.categoryId}` === itemCategory){
            return (
                <div className="recipe-item" key={index}>
                    <h4 key={item.id}><Link to={`/${item.categoryMain}/${item.categoryId}/${item.id}`}>{item.name}</Link></h4>
                    <div className="recipe-item-picture"><img src={item.imgLink} alt='some value'/></div>
                    <div className="recipe-item-category">
                        <Link to={`/${item.categoryMain}`}><span>{item.category}</span></Link>
                        <Link to={`/${item.categoryMain}/${item.categoryId}`}><span> • {item.childСategory}</span></Link>
                    </div>
                    <div className="recipe-item-description"><strong>Время приготовления: </strong> <br/> {item.descriptionShort}</div>
                </div>
            );
        }
        return null;
    }); 

    return (
        <div className="category-page">
            {categoryName}
            <div className="recipe-wrapper"> {categoryrecipe} </div> 
        </div>
    );
};
export default  Category;