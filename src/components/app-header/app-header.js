import React from 'react';
import './app-header.css';
import {Link} from 'react-router-dom';

const AppHeader = () => {
  return (
    <div className="app-header">
      <div className="container">
        <div className="logo"> <Link to='/'>Recipes</Link> </div>
      </div>
    </div>
  );
};
export default AppHeader;