import React from 'react';
import { Link } from 'react-router-dom';
import './breadcrumbs.css';

const Breadcrumbs = ({pathname, category}) =>{
    const  pathnameArray = pathname.split("/");
    const pathName = pathnameArray.filter((v => v));

    const liCategory = category.map( (item, index) => {
        if(item.link === pathName[0]){
            return(
                <li key={index} className="breadcrumbs__item">
                    <Link to={'/'+ item.link}>{item.name} </Link>
                </li>                    
            );
        }
        return null;
    });

    const liChildCategory = category.map( (el) => {
        const child = el.items;
        return child.map( (ch, index)=>{
            if(ch.categoryName === pathName[1]){
                return(
                    <li key={index} className="breadcrumbs__item">
                        <Link to={'/'+ ch.link}>{ch.name}</Link>
                    </li>
                );
            }
            return null;
        });
    });

    return(
        <ul className="breadcrumbs">
            <li><Link to="/">Главная</Link></li>
            {liCategory}
            {liChildCategory}
        </ul>
    );

}
export default  Breadcrumbs;